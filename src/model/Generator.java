package model;

public class Generator {
    private int a;
    private int b;
    private int x;
    private final int N = 15;

    public Generator() {
    }

    public Generator(int a, int b, int x) {
        this.a = a;
        this.b = b;
        this.x = x;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int generate(int a, int b, int x) {
        return this.x =(a * x + b) % N;
    }

}
