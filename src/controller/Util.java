package controller;

import model.Generator;
import model.Sequence;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Util {

    private static Generator firstGenerator = new Generator();
    private static Generator secondGenerator = new Generator();
    private static Sequence sequence = new Sequence();

    static int[] generateNumbers(int max) {
        Random random = new Random();
        int first = random.nextInt(max);
        int second = random.nextInt(max);
        while (second == first) {
            second = random.nextInt(max);
        }
        return new int[]{first, second};
    }

    public static Sequence run(int length, int[][] B) throws IOException {
        int[] coef;
        coef = generateNumbers(B.length);
        int[] start = generateNumbers(16);
        firstGenerator.generate(B[coef[0]][0], B[coef[0]][1], start[0]);
        secondGenerator.generate(B[coef[1]][0], B[coef[1]][1], start[1]);
        sequence.calculate(firstGenerator.getX(), secondGenerator.getX());
        for (int i = 0; i < length - 1; i++) {
            coef = generateNumbers(B.length);
            firstGenerator.generate(B[coef[0]][0], B[coef[0]][1], firstGenerator.getX());
            secondGenerator.generate(B[coef[1]][0], B[coef[1]][1], secondGenerator.getX());
            sequence.calculate(firstGenerator.getX(), secondGenerator.getX());
        }
        sequence.print();
        return sequence;
    }

    public static int[][] createB() {
        int[][] B = new int[5][2];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Необходимо ввести пять пар чисел");
        for (int i = 0; i < 5; i++) {
            System.out.printf("Введите %d пару\n", i + 1);
            System.out.print("Первое число ");
            B[i][0] = scanner.nextInt();
            System.out.print("Второе число ");
            B[i][1] = scanner.nextInt();
        }
        return B;
    }

    public static int period(Sequence sequence) {
        int middle = sequence.size() / 2;
        int period = 0;
        String seq = sequence.toString();
        while (period == 0) {
            String substr1 = seq.substring(0, middle);
            String substr2 = seq.substring(middle);
            int index = substr2.indexOf(substr1);
            System.out.println(index + " " + middle);
            if (index != -1) {
                period = middle + index;
            } else {
                if(middle>0) {
                    middle -= 1;
                }
            }
        }
        return period;
    }


    public static String removeChar(String s, char c) {
        String r = "";
        for (int i = 0; i < s.length(); i++) {

            if (s.charAt(i) != c) r += s.charAt(i);

        }
        return r;
    }
}
